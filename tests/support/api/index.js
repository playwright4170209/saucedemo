const { expect } = require('@playwright/test')

export class Api {

    constructor(request) {
        this.request = request
        this.token = undefined
    }

    async setToken() {
        const response = await this.request.post('http://localhost:9002/api/account/token/', {
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json, text/plain, */*"
            },
            data: {
                username: process.env.USER_NAME_API,
                password: process.env.USER_PASSWORD_API
            }
        })

        expect(response.ok()).toBeTruthy()
        const body = JSON.parse(await response.text())
        this.token = 'Bearer ' + body.token.access
    }

    async getCompanyIdByName(companyName) {
        const response = await this.request.get('http://localhost:9002', {
            headers: {
                Authorization: this.token
            },
            params: {
                name: companyName
            }
        })

        expect(response.ok()).toBeTruthy()

        const body = JSON.parse(await response.text())
        return body.data[0].id
    }

    async postCompany(company) {
        const response = await this.request.post('http://localhost:9002/api/core/company/', {
            headers: {
                Authorization: this.token,
                ContentType: 'application/json',
                Accept: 'application/json, text/plain, */*'
            },
            data: {
                name: company.description,
                cnpj: company.cnpj
            }

        })

        expect(response.ok()).toBeTruthy()
    }

    async postUser(name, email, username, isSuperuser) {
        const response = await this.request.post(process.env.BASE_API + '/api/account/user/', {
            headers: {
                Authorization: this.token,
                ContentType: 'application/json',
                Accept: 'application/json, text/plain, */*'
            },
            data: {
                name: name,
                email: email,
                username: username,
                isSuperuser: isSuperuser,
                isActive: true
            }

        })

        expect(response.ok()).toBeTruthy()
    }



}