const { expect } = require('@playwright/test')

export class Login {

    constructor(page) {
        this.page = page
    }

    async do(username, password) {
        await this.visit()
        await this.submit(username, password)
        await this.isLoggedIn(username)
    }

    async visit() {
        await this.page.goto('/')
    }

    async submit(username, password) {
        await this.page.getByText('Usuário').fill(username)
        await this.page.getByText('Senha').fill(password)
        await this.page.getByRole('button', { name: 'Entrar' }).click()
    }

    async alertHaveText(text) {
        const alert = this.page.locator('span[class$=alert]')
        await expect(alert).toHaveText(text)
    }

    async isLoggedIn(username) {
        await expect(this.page.getByText('Bem vindo ao ePeople')).toHaveCount(2)
    }
}