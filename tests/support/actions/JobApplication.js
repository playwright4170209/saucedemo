const { expect } = require('@playwright/test')

export class JobApplication {

    constructor(page) {
        this.page = page
    }

    async do(person) {
        await this.visit()
        await this.stage1_identity(person.cpf)
        await this.stage2_term_accept()
        await this.stage3_identification(person)
        await this.stage4_documentation(person)
        await this.stage5_intended_role(person)
        await this.stage6_contact(person)
        await this.stage7_address(person)
        await this.stage8_family_data(person)
        await this.stage9_extra_nformation(person)
        await this.stage10_Schooling(person)
        await this.stage11_language(person)
        await this.stage12_work_experience(person)
    }
    
    async existing_candidate(person) {
        await this.visit()
        await this.stage1_identity(person.cpf)
        await this.aplly_new_selecion()

        const locator = this.page.locator('mat-dialog-container').filter({ hasText: 'Seleções disponíveis' })
        await locator.locator('[formcontrolname="selection"]').click()
        await this.page.getByRole('option', { name: person.intended_role }).click()
        await this.confirm()
        //VALIDAR SE O CURSO FOI ADICIONADO
        await this.validate_application()
    }

    async aplly_new_selecion(){
        await this.page.locator('#save').click()
    }

    async visit() {
        await this.page.goto('/#/job_application')
    }


    async next_page() {
        await this.page.getByRole('button', { name: "Próximo" }).or(this.page.getByRole('button', { name: "Next" })).click()
    }

    async confirm() {
        await this.page.getByRole('button', { name: "Confirmar" }).or(this.page.getByRole('button', { name: "Confirm" })).click()
    }

    async finish() {
        await this.page.getByRole('button', { name: "Finalizar" }).or(this.page.getByRole('button', { name: "Finish" })).click()
    }
    async go_to_page() {
        await this.page.getByRole('button', { name: 'Ir para a página' }).or(this.page.getByRole('button', { name: 'Go to page ' })).click()
    }

    async stage1_identity(cpf) {
        await this.page.locator('[formcontrolname="cpf"]').fill(cpf)
        await this.page.getByRole('button', { name: 'Ir para a página' }).or(this.page.getByRole('button', { name: 'Go to page ' })).click()

    }

    async stage2_term_accept() {
        await this.page.locator('[formcontrolname="terms_accept"]').click()
        await this.next_page()
    }

    async validate_application() {
        await expect(this.page.getByText('Obrigado')).toBeVisible()
        await expect(this.page.getByText('Proposta de emprego enviada com sucesso.')).toBeVisible()
    }

    async stage3_identification(person) {
        const locator = this.page.getByRole('tabpanel').filter({ hasText: '1/10' })
        await locator.locator('[formcontrolname="name"]').fill(person.name)
        await locator.locator('[formcontrolname="birth_date"]').fill(person.birth_date)
        await locator.locator('[formcontrolname="birth_uf"]').click()
        await this.page.getByRole('option', { name: person.birth_state }).click()
        await locator.locator('[formcontrolname="birth_city"]').fill(person.birth_city)
        await this.next_page()
    }

    async stage4_documentation(person) {
        const locator = this.page.getByRole('tabpanel').filter({ hasText: '2/10' })
        await locator.locator('[formcontrolname="ctps"]').fill(person.ctps)
        await locator.locator('[formcontrolname="ctps_series"]').fill(person.ctps_series)
        await locator.locator('[formcontrolname="ctps_emission_date"]').fill(person.ctps_series)
        await locator.locator('[formcontrolname="ctps_uf"]').click()
        await this.page.getByRole('option', { name: person.ctps_uf }).click()
        await locator.locator('[formcontrolname="rg"]').fill(person.rg)
        await locator.locator('[formcontrolname="rg_emission_date"]').fill(person.ctps_emission)
        await this.next_page()
    }


    async stage5_intended_role(person) {
        const locator = this.page.getByRole('tabpanel').filter({ hasText: '3/10' })
        if (person.business_hours) {
            await locator.locator('[formcontrolname="is_business_hours"] button').click()
        }
        if (person.first_shift) {
            await locator.locator('[formcontrolname="is_first_shift"] button').click()
        }
        if (person.second_shift) {
            await locator.locator('[formcontrolname="is_second_shift"] button').click()
        }
        if (person.third_shift) {
            await locator.locator('[formcontrolname="is_third_shift"] button').click()
        }
        if (person.other_shift) {
            await locator.locator('[formcontrolname="is_other_shift"] button').click()
        }
        await locator.locator('[formcontrolname="selection"]').click()
        await this.page.getByRole('option', { name: person.intended_role }).click()
        await locator.getByText('add').click()
        //VALIDAR SE A SELEÇÃO  FOI ADICIONADA
        await this.next_page()
    }

    async stage6_contact(person) {
        const locator = this.page.getByRole('tabpanel').filter({ hasText: '4/10' })
        await locator.locator('[formcontrolname="email"]').fill(person.email)
        await locator.locator('[formcontrolname="phone"]').fill(person.phone)
        await locator.locator('[formcontrolname="phone2"]').fill(person.phone2)
        await this.page.waitForTimeout(500)
        await this.next_page()
    }

    async stage7_address(person) {
        const locator = this.page.getByRole('tabpanel').filter({ hasText: '5/10' })
        await locator.locator('[formcontrolname="cep"]').fill(person.cep)
        await locator.locator('[formcontrolname="street"]').fill(person.street)
        await locator.locator('[formcontrolname="number"]').fill(person.number)
        await locator.locator('[formcontrolname="complement"]').fill(person.complement)
        await locator.locator('[formcontrolname="city"]').fill(person.city)
        await locator.locator('[formcontrolname="uf"]').click()
        await this.page.getByRole('option', { name: person.uf }).click()
        await this.next_page()
    }

    async stage8_family_data(person) {
        const locator = this.page.getByRole('tabpanel').filter({ hasText: '6/10' })
        if (person.has_relative) {
            await locator.locator('[formcontrolname="has_relative_in_company"] button').click()
            await locator.locator('[formcontrolname="relatives_name"]').fill(person.relatives_name)
            await locator.locator('[formcontrolname="relatives_kinship"]').fill(person.relatives_kinship)
            await locator.locator('[formcontrolname="relatives_sector"]').fill(person.relatives_sector)
        }
        if (person.has_indication) {
            await locator.locator('[formcontrolname="has_indication"] button').click()
            await locator.locator('[formcontrolname="indicator_name"]').fill(person.indicator_name)
            await locator.locator('[formcontrolname="indicator_sector"]').fill(person.indicator_sector)
        }
        await this.page.waitForTimeout(500)
        await this.next_page()
    }

    async stage9_extra_nformation(person) {
        const locator = this.page.getByRole('tabpanel').filter({ hasText: '7/10' })
        if (person.worked_in_company) {
            await locator.locator('[formcontrolname="has_worked_in_company"] button').click()
            await locator.locator('[formcontrolname="when_worked_in_company"]').fill(person.worked_in_company_date)
            await locator.locator('[formcontrolname="reason_leaving_company"]').fill(person.reason_leaving_company)
        }

        if (person.international_experience) {
            await locator.locator('[formcontrolname="has_international_experience"] button').click()
            await locator.locator('[formcontrolname="international_experience_country"]').fill(person.international_experience_country)
            await locator.locator('[formcontrolname="when_worked_in_company"]').fill(person.worked_in_company_date)
            await locator.locator('[formcontrolname="travel_reasons"]').click()
            await this.page.getByRole('option', { name: person.travel_reasons }).click()
            await locator.locator('[formcontrolname="travel_period"]').click()
            await this.page.getByRole('option', { name: person.travel_period }).click()
        }
        await this.next_page()
    }

    async stage10_Schooling(person) {
        const locator = this.page.getByRole('tabpanel').filter({ hasText: '8/10' })
        await locator.locator('[formcontrolname="name"]').fill(person.schooling.name)
        await locator.locator('[formcontrolname="level"]').click()
        await this.page.getByRole('option', { name: person.schooling.level }).click()
        await locator.locator('[formcontrolname="situation"]').click()
        await this.page.getByRole('option', { name: person.schooling.situation }).click()
        await locator.locator('[formcontrolname="period"]').fill(person.schooling.period)
        await locator.getByText('add').click()
        await this.next_page()
    }

    async stage11_language(person) {
        const locator = this.page.getByRole('tabpanel').filter({ hasText: '9/10' })
        await locator.locator('[formcontrolname="language"]').fill(person.language.language)
        await locator.locator('[formcontrolname="reading_level"]').click()
        await this.page.getByRole('option', { name: person.language.reading_level }).click()
        await expect(await this.page.locator('.cdk-overlay-container')).not.toContainText(person.language.reading_level)

        await locator.locator('[formcontrolname="writing_level"]').click()
        await this.page.getByRole('option', { name: person.language.writing_level }).click()
        await expect(await this.page.locator('.cdk-overlay-container')).not.toContainText(person.language.writing_level)

        await locator.locator('[formcontrolname="speaking_level"]').click()
        await this.page.getByRole('option', { name: person.language.writing_level }).click()
        await locator.getByText('add').click()
        //VALIDAR SE O IDIOMA FOI ADICIONADO
        await this.next_page()
    }

    async stage12_work_experience(person) {
        const locator = this.page.getByRole('tabpanel').filter({ hasText: '10/10' })
        if (person.work_experience.has_work_experience) {
            await locator.locator('[formcontrolname="company_name"]').fill(person.work_experience.company_name)
            if (person.work_experience.current_company) {
                await locator.locator('[formcontrolname="current"] button').click()
                await locator.locator('[formcontrolname="admission_date"]').fill(person.work_experience.admission)
            } else {
                await locator.locator('[formcontrolname="admission_date"]').fill(person.work_experience.admission)
                await locator.locator('[formcontrolname="dismissal_date"]').fill(person.work_experience.admission)
            }
            await locator.locator('[formcontrolname="position"]').fill(person.work_experience.position)
            await locator.locator('[formcontrolname="main_activities"]').fill(person.work_experience.main_activities)
            await locator.getByText('add').click()
            //VALIDAR SE O IDIOMA FOI ADICIONADO
        }
        await this.finish()
        await this.validate_application()
    }
}