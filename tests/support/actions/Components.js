const { expect } = require('@playwright/test')

export class Toast {

    constructor(page) {
        this.page = page
    }

    async haveText(message) {
        await expect(this.page.locator('#toast-container')).toHaveText(message)
    }

}


export class Button {

    constructor(page) {
        this.page = page
    }

    async add() {
        await this.page.getByRole('button', { name: "Novo" }).click()
    }

    async save() {
        await this.page.locator('#save').click()
    }
}