export class Basic {

    constructor(page) {
        this.page = page
    }

    async newCompany(description, cnpj) {
        await this.page.goto('/#/basic/company');
        // await this.page.getByRole('button', {name:"Novo"}).click()
        await this.page.button.add()
        await this.page.locator('[formcontrolname="name"]').fill(description)
        await this.page.locator('[formcontrolname="cnpj"]').fill(cnpj)
        await this.page.button.save()
    }

    async newSector(description) {
        await this.page.goto('/#/basic/sector');
        await this.page.button.add()
        await this.page.locator('[formcontrolname="name"]').fill(description)
        await this.page.button.save()
    }

    async newRole(description) {
        await this.page.goto('/#/basic/role');
        await this.page.button.add()
        await this.page.getByText('Descrição').fill(description)
        await this.page.button.save()
        await this.page.toast.haveText(' Sucesso  Salvo com sucesso ')

        await this.page.getByRole('button', { name: 'Adicionar' }).click()
        const dialog = this.page.locator('mat-dialog-container').filter({ hasText: 'Empresa' })
        await dialog.getByLabel('Empresa').click()
        await this.page.getByText('FPF-TECH').click()
        await dialog.getByLabel('Setor').click()
        await this.page.getByText('PCP').click()
        await dialog.getByRole('submit')
        await dialog.locator('button[type="submit"]').click()
    }

    async newSelection(selection, opened) {
        await this.page.goto('/#/human_resources/selection');
        await this.page.button.add()
        await this.page.locator('[formcontrolname="description"]').fill(selection)
        await this.page.locator('[formcontrolname="company_role"]').click()
        await this.page.getByText('Analista planejamento JR - PCP - FPF-TECH').click()
        if (opened) {
            await this.page.getByRole('switch').click();
        }
        await this.page.button.save()
    }
}