const { test: base, expect } = require('@playwright/test')

const { Login } = require('./actions/Login')
const { Toast, Button } = require('./actions/Components')
const { Basic } = require('./actions/Basic')
const { JobApplication } = require('./actions/JobApplication')

const { Api } = require('./api')

const test = base.extend({
    page: async ({ page }, use) => {
        const context = page

        context['login'] = new Login(page)
        context['toast'] = new Toast(page)
        context['basic'] = new Basic(page)
        context['button'] = new Button(page)
        context['jobApplication'] = new JobApplication(page)

        await use(context)
    },
    request: async ({ request }, use) => {
        const context = request
        context['api'] = new Api(request)

        await context['api'].setToken()

        await use(context)
    }
})

export { test, expect }