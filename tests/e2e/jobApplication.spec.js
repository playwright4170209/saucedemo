import { test } from '../support';

const data = require('../support/fixtures/person.json')
import { executeSQL } from '../support/data_base'


test.beforeAll(async () => {
  await executeSQL(`DELETE FROM WORK_EXPERIENCE;`)
  await executeSQL(`DELETE FROM LANGUAGE;`)
  await executeSQL(`DELETE FROM SCHOOLING_COURSE;`)
  await executeSQL(`DELETE FROM PERSON_SELECTION;`)
  await executeSQL(`DELETE FROM PERSON;`)
})


test('Cadastro seleção nova pessoa', async ({ page }) => {
  const person = data.flavia
  await page.jobApplication.do(person)
});

test('Cadastro seleção pessoa existente', async ({ page }) => {
  const person = data.flavia
  await page.jobApplication.existing_candidate(person)
});