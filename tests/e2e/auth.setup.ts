import { expect, test as setup } from '@playwright/test';
require('dotenv').config();


const authFile = 'playwright/.auth/user.json';

setup('authenticate', async ({ page }) => {
    await page.goto('/')
    await page.getByText('Usuário').fill('admin')
    await page.getByText('Senha').fill('123456')
    await page.getByRole('button', { name: 'Entrar' }).click()
    await expect(page.getByText('Bem vindo ao ePeople')).toHaveCount(2)


    await page.context().storageState({ path: authFile });
});