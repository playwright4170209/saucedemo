import { test, expect } from '../support';
import { executeSQL } from '../support/data_base'

const data = require('../support/fixtures/basic.json')

test.beforeAll(async () => {
  await executeSQL(`DELETE FROM PERSON_SELECTION;`)
  await executeSQL(`DELETE FROM SELECTION;`)
  await executeSQL(`DELETE FROM COMPANY_ROLE;`)
  await executeSQL(`DELETE FROM SECTOR_USER;`)
  await executeSQL(`DELETE FROM COMPANY;`)
  await executeSQL(`DELETE FROM SECTOR;`)
  await executeSQL(`DELETE FROM ROLE;`)
})

test('Acessar página inicial', async ({ page }) => {
  await page.goto('/');
  await expect(page.getByText('Bem vindo ao ePeople')).toHaveCount(2)
});

test('Cadastrar empresa', async ({ page }) => {
  const company = data.company_create
  await page.basic.newCompany(company.description, company.cnpj)
  await page.toast.haveText('Sucesso  Salvo com sucesso')
});

test('Não deve cadastrar empresa com nome duplicado', async ({ page, request }) => {
  const company = data.company_duplicate
  await request.api.postCompany(company)
  await page.basic.newCompany(company.description, company.cnpj)
  await page.toast.haveText('Requisição inválida  Já existe uma empresa com este nome ')
});

test('Cadastrar setor', async ({ page }) => {
  const sector = data.sector
  await page.basic.newSector(sector.description)
  await page.toast.haveText('Sucesso  Salvo com sucesso')
});

test('Cadastrar função', async ({ page }) => {
  const role = data.role
  await page.basic.newRole(role.description)
  await page.toast.haveText('Sucesso  Salvo com sucesso')
});


test('Cadastrar seleção com status aberto', async ({ page }) => {
  const selection = data.role_selection
  await page.basic.newSelection(selection.description, selection.opened)
  await page.toast.haveText('Sucesso  Salvo com sucesso')
});